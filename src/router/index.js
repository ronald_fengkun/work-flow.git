import Vue from 'vue'
import VueRouter from 'vue-router'
import WorkFlowPage from '../components/WorkFlow/WorkFlowPage.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: WorkFlowPage
  }
]

const router = new VueRouter({
  routes
})

export default router
